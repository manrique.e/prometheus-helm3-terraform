provider "kubernetes" {
  config_path = "kube-config.yaml"
}

resource "null_resource" "helmChart" {

  provisioner "local-exec" {
    command = "helm repo add stable https://kubernetes-charts.storage.googleapis.com/;"
  }
}

resource "kubernetes_namespace" "monitoring" {
  metadata {
    name = "monitoring"
  }
  depends_on = [null_resource.helmChart]
}

resource "kubernetes_secret" "tls_cert" {

  metadata {
    name      = "certs"
    namespace = "monitoring"
  }
  data = {
    "tls.crt" = var.tls_cert
    "tls.key" = var.tls_key
  }
  type       = "kubernetes.io/tls"
  depends_on = [kubernetes_namespace.monitoring]
}

module "prometheus-helm-install" {
  source = "./prometheus-helm-install"
  
  alertmanagerDomain = var.alertmanagerDomain
  prometheusDomain   = var.prometheusDomain
  storageClass       = var.storageClass
  priority           = kubernetes_secret.tls_cert.id

}



