variable "tls_cert" {
  type = string
}

variable "tls_key" {
  type = string
}

variable "alertmanagerDomain" {
  type = string
}

variable "prometheusDomain" {
  type = string
}

variable "storageClass" {
  type = string
}


