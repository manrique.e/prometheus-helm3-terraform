
resource "null_resource" "prometheusInstall" {
  
  #provisioner "local-exec" {
    #command = "echo \"${templatefile("${path.module}/templates/values.tmpl", {
    #  alertmanagerDomain = var.alertmanagerDomain,
    #  prometheusDomain   = var.prometheusDomain,
    #  storageClass       = var.storageClass,
    #})}\" > values.yaml" 
  #}
  
  provisioner "local-exec" {
    command = <<EOT
      cat ${path.module}/templates/prometheus-operator-stable-values.yml | \
      sed 's/__alertmanagerDomain__/${var.alertmanagerDomain}/g' |\
      sed 's/__prometheusDomain__/${var.prometheusDomain}/g' | \
      sed  's/__storageClass__/${var.storageClass}/g' \
      > ${path.module}/templates/values.yaml;
    EOT
  }

  provisioner "local-exec" {
    command = <<EOT
helm upgrade --install prometheus stable/prometheus-operator --namespace monitoring -f ${path.module}/templates/values.yaml;
EOT
  }
  depends_on = [var.priority]

}


