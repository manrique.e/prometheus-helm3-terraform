variable "alertmanagerDomain" {
  type = string
}

variable "prometheusDomain" {
  type = string
}

variable "storageClass" {
  type = string
}

variable "priority" {
  type = string
}