module "monitoring" {
  source = "./modules/monitoring"

  tls_cert           = var.tls_cert
  tls_key            = var.tls_key
  alertmanagerDomain = var.alertmanagerDomain
  prometheusDomain   = var.prometheusDomain
  storageClass       = var.storageClass
}